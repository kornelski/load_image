fn main() {
    let path = std::env::args().nth(1).expect("File name");
    load_image::load_path(path).unwrap();
}
